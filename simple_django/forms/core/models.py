from django.db import models
from django.db.models.signals import pre_save


class Student(models.Model):
    phone = models.CharField(max_length=255, verbose_name="Телефон", null=True)
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    last_name = models.CharField(max_length=255, verbose_name="Фамилия", null=True)
    age = models.IntegerField(null=True)

    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"

    def clean(self):
        self.first_name = self.first_name.capitalize()
        self.last_name = self.last_name.capitalize()

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


class Teacher(models.Model):
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    last_name = models.CharField(max_length=255, verbose_name="фамилия", null=True)

    class Meta:
        verbose_name = "Учитель"
        verbose_name_plural = "Учителя"

    def clean(self):
        self.first_name = self.first_name.capitalize()
        self.last_name = self.last_name.capitalize()

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


class Group(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название группы")
    students = models.ManyToManyField("core.Student", blank=True)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = "Группы"

    def __str__(self):
        return self.name


def send_notify(instance, **kwargs):
    from core.utils import notify
    notify(instance)


pre_save.connect(send_notify, sender=Student)
pre_save.connect(send_notify, sender=Teacher)
