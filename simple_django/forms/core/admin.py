from django.contrib import admin
from core.models import Student, Group, Teacher


class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'age', 'phone', )
    search_fields = ('last_name', )
    list_filter = ('group', )


admin.site.register(Group, )
admin.site.register(Student, StudentAdmin, )
admin.site.register(Teacher)
