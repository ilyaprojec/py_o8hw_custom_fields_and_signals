from django import forms
from core.fields import PhoneField
from core.models import Group, Student, Teacher


class GroupForm(forms.ModelForm):

    class Meta:
        model = Group
        fields = '__all__'
        widgets = {
            'students': forms.widgets.CheckboxSelectMultiple()
        }


class StudentForm(forms.ModelForm):
    phone = PhoneField()

    class Meta:
        model = Student
        fields = '__all__'


class TeacherForm(forms.ModelForm):

    class Meta:
        model = Teacher
        fields = '__all__'
